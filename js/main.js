$('.portfolioicon').on('click', function(x){
    showPortfolioItem(x);
});

function showPortfolioItem(x){
    var currentPortfolioIcon = $(x.currentTarget).clone();
    $(currentPortfolioIcon).removeClass('portfolioicon');
    var currentClassName = $(currentPortfolioIcon).attr('class');
    $(currentPortfolioIcon).addClass('currentportfolioicon');
    $(currentPortfolioIcon).css({opacity: 0, transform: 'scale(0.5, 0.5)'});
    $('.currenticoncontainer').html('');
    $('.currenticoncontainer').append(currentPortfolioIcon);
    
    $('.currentdetailscontainer').find('.bodytext').html('');
    var currentPortfolioText = $('.' + currentClassName + 'text').clone(true);
    $('.currentdetailscontainer').find('.bodytext').append(currentPortfolioText);
    var portfolioBackButton = $('.portfoliobacktext').clone(true);
    $('.currentdetailscontainer').find('.bodytext').append(portfolioBackButton);
    
    $('.selecticontext').transition({opacity: 0, duration: 300, queue: false});
    $('.portfolioicon').transition({opacity: 0, transform: 'scale(0.5, 0.5)', duration: 500, queue: false}, function(){
        $('.iconscontainer').css({display: 'none'});
        $('.detailscontainer').css({display: 'block'});
        $('.detailscontainer').transition({opacity: 1, duration: 300, queue: false});
        $('.currentportfolioicon').transition({opacity: 1, transform: 'scale(1, 1)', duration: 500, queue: false});
    });
}

function goBackToHome(){
    $('.detailscontainer').transition({opacity: '0', duration: 300, queue: false});
    $('.currentportfolioicon').transition({opacity: 0, transform: 'scale(0.5, 0.5)', duration: 500, queue: false}, function(){
        $('.detailscontainer').css({display: 'none'});
        $('.iconscontainer').css({display: 'block'});
        $('.portfoliosubtitletext').css({display: 'block'});
        $('.selecticontext').transition({opacity: 1, duration: 300, queue: false});
        $('.portfoliosubtitletext').transition({opacity: 1, duration: 300, queue: false});
        $('.portfolioicon').transition({opacity: 1, transform: 'scale(1, 1)', duration: 500, queue: false});
    });
}

$('.portfoliobacktext').on('click', function(){
    goBackToHome();
});

$(window).on('hashchange', function(x) {
    var hash = window.location.hash;
    if (hash === '#home'){
        goBackToHome();
    } else {
        
    }
});

if (window.location.hash != ""){
    $('.' + window.location.hash.substring(1)).click();
    var offset = $('.portfoliosubtitletext').offset();
    $('html, body').animate({
        scrollTop: offset.top,
    });
}